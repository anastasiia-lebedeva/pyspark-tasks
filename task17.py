# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task17')
         .getOrCreate())

employee_path = 'input/employees/employees'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))

# employee_df.show()
# employee_df.printSchema()

(employee_df
 .select(
    f.col('employee_id'),
    f.col('last_name'),
    f.col('salary'))
 .filter(f.col('salary') > employee_df.select(f.avg('salary')).collect()[0][0])
 .write
 .format('avro')
 .mode('overwrite')
 .save('17_res'))

# employee_df.registerTempTable('employee')
# (spark.sql('''
#         SELECT employee_id, last_name, salary
#           FROM employee e
#          WHERE int(salary) >
#                (SELECT AVG(int(salary))
#                   FROM employee)
#     ''').show())
#     .write
#     .format('avro')
#     .mode('overwrite')
#     .save('17_res'))

spark.stop()
