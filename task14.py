# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task14')
         .getOrCreate())

employee_path = 'input/employees/employees'
department_path = 'input/departments/departments'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))
department_df = (spark.read
                 .format('csv')
                 .option('header', 'true')
                 .option('sep', ',')
                 .load(department_path))

# employee_df.show()
# department_df.show()

(employee_df.alias('e')
 .join(department_df, employee_df.DEPARTMENT_ID == department_df.DEPARTMENT_ID)
 .join(employee_df.alias('c'), f.col('e.DEPARTMENT_ID') == f.col('c.DEPARTMENT_ID'))
 .select(
    department_df.DEPARTMENT_ID.alias('dep_id'),
    f.col('e.LAST_NAME').alias('elast'),
    f.col('c.LAST_NAME').alias('clast')
)
 .groupBy(f.col('dep_id'), f.col('elast'))
 .agg(f.collect_set(f.col('clast')).cast('string'))
 .toDF('DEPARTMENT', 'EMPLOYEE', 'COLLEAGUE')
 .write
 .format('csv')
 .mode('overwrite')
 .option('header', 'true')
 .option('sep', '\\\\')
 .save('14_res'))

spark.stop()
