# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

# Expirement with Spark SQL

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task13')
         .getOrCreate())

employee_path = 'input/employees/employees'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))

employee_df.registerTempTable('employee')

# Avro field names can't contain the # character,
# that's why I rename columns to EmpID and MgrID

(spark.sql('''
    SELECT e.last_name Employee, 
           e.employee_id as `EmpID`, 
           m.last_name Manager,
           m.employee_id `MgrID`
      FROM employee e
           LEFT JOIN employee m
           ON e.manager_id = m.employee_id''')
 .write
 .format('avro')
 .mode('overwrite')
 .option('compression', 'snappy')
 .save('13_res'))

spark.stop()
