# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task22')
         .getOrCreate())

employee_path = 'input/employees/employees'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))

employee_df.registerTempTable('employee')

(spark.sql('''
        SELECT e.last_name
          FROM employee e
         WHERE e.salary >
               (SELECT max(salary)
                  FROM employee
                 WHERE department_id = 60)
    ''')
 .write
 .format('parquet')
 .saveAsTable('study_dev.alebedeva_sol_22'))

spark.stop()
