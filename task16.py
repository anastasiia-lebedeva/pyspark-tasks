# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task16')
         .getOrCreate())

employee_path = 'input/employees/employees'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))

employee_df.registerTempTable('employee')
(spark.sql('''
        SELECT e.last_name EmployeeLastName, 
               e.hire_date EmployeeHireDate, 
               m.last_name ManagerLastName, 
               m.hire_date ManagerHireDate
          FROM employee e
               JOIN employee m
                 ON e.manager_id = m.employee_id
         WHERE to_date(cast(unix_timestamp(e.hire_date, 'dd.MM.yy') as timestamp)) <
               to_date(cast(unix_timestamp(m.hire_date, 'dd.MM.yy') as timestamp))
    ''')
 .write
 .format('orc')
 .mode('overwrite')
 .option('compression', 'zlib')  # IllegalArgumentException: Codec [gzip] is not available.
 # Available codecs are uncompressed, lzo, snappy, zlib, none.
 .save('16_res'))

spark.stop()
