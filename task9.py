# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f

spark = SparkSession.builder \
    .master('local[*]') \
    .appName('lebedeva-spark-tasks') \
    .getOrCreate()

dep_path = 'input/departments/departments'
dep_df = spark.read \
    .format('csv') \
    .option('header', 'true') \
    .load(dep_path)

loc_path = 'input/locations/part-00000-f3626ac3-ae1a-49d5-8aa5-760f2e41b579-c000.gz.parquet'
loc_df = spark.read \
    .format('parquet') \
    .load(loc_path)

country_path = 'input/countries/part-00000-ebe0451d-1509-4b7e-a7fd-abeb9e5488ef-c000.snappy.orc'
country_df = spark.read \
    .format('orc') \
    .load(country_path)

# dep_df.show()
# loc_df.show()
# country_df.show()

res_df = dep_df \
    .join(loc_df, 'location_id', 'left') \
    .join(country_df, 'country_id') \
    .groupBy('department_id') \
    .agg(
        f.first('LOCATION_ID').alias('location_id'),
        f.first('STREET_ADDRESS').alias('street_address'),
        f.first('CITY').alias('city'),
        f.first('STATE_PROVINCE').alias('state_province'),
        f.first('COUNTRY_NAME').alias('country_name')
    ) \
    .select(
        # f.col('department_id'),
        f.col('location_id'),
        f.col('street_address'),
        f.col('city'),
        f.col('state_province'),
        f.col('country_name')
    )

res_df.write \
    .mode('overwrite') \
    .parquet('9_res')

spark.stop()