# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task25')
         .getOrCreate())

employee_path = 'input/employees/employees'
employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))

employee_df.registerTempTable('employee')

assert len(spark.sql('''
        SELECT *
          FROM employee
         WHERE year(cast(unix_timestamp(hire_date, 'dd.MM.yy') as timestamp)) > 2022
    ''').collect()) == 0

(spark.sql('''
        SELECT year(cast(unix_timestamp(e.hire_date, 'dd.MM.yy') as timestamp)) year,
               count(1) employee_count
          FROM employee e
         GROUP BY year(cast(unix_timestamp(e.hire_date, 'dd.MM.yy') as timestamp))
    ''')
 .write
 .format('csv')
 .saveAsTable('study_dev.alebedeva_sol_25'))

spark.stop()
