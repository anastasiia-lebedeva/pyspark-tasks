# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f
from pyspark.sql.types import IntegerType

# read data and start spark_session
path_to_empl = 'input/employees/employees'  # '~/data_for_exam/employees/employees'
path_to_hist = 'input/job_history/job_history'
spark = SparkSession.builder \
    .master('local[*]') \
    .appName('lebedeva-spark-tasks') \
    .getOrCreate()
empl_df = spark.read \
    .format('csv') \
    .option('header', 'true') \
    .option('sep', '\t') \
    .load(path_to_empl)
hist_df = spark.read \
    .format('json') \
    .load(path_to_hist)

# join data by employee_id, add columns with last 2 digits of year, ex: 19.01.21 -> 21 and cast to int
res_df = empl_df \
    .join(hist_df, empl_df.EMPLOYEE_ID == hist_df.employee_id) \
    .withColumn('start_year', f.substring(f.col('start_date'), -2, 2).cast(IntegerType())) \
    .withColumn('end_year', f.substring(f.col('end_date'), -2, 2).cast(IntegerType())) \
    .withColumn('cur_year', f.substring(f.current_date(), 3, 2).cast(IntegerType()))

# if [start_year or end_year] > cur_year then it's 20 century and write 19xx, else it's 21 century and write 20xx
# Ex: 97 -> 1997; 5 -> 2005
res_df = res_df \
    .select(
        f.col('last_name'),
        f.when(
            f.col('start_year') > f.col('cur_year'),
            f.to_date(f.date_format(f.to_date(f.col('start_date'), 'dd.MM.yy'), '19yy-MM-dd'))
        ).otherwise(
            f.to_date(f.date_format(f.to_date(f.col('start_date'), 'dd.MM.yy'), '20yy-MM-dd'))
        ).alias('start'),
        f.when(
            f.col('end_year') > f.col('cur_year'),
            f.to_date(f.date_format(f.to_date(f.col('end_date'), 'dd.MM.yy'), '19yy-MM-dd'))
        ).otherwise(
            f.to_date(f.date_format(f.to_date(f.col('end_date'), 'dd.MM.yy'), '20yy-MM-dd'))
        ).alias('end')
    )

# for each human calculate month between dates, sum it ('cause one human can have few rows) and then round; sort by month_worked
res_df = res_df \
    .groupBy(f.col('LAST_NAME')) \
    .agg(f.round(f.sum(f.months_between(res_df.end, res_df.start))).alias('MONTH_WORKED')) \
    .repartition(1) \
    .sort(f.col('MONTH_WORKED'))

res_df.write \
    .option('sep', ';') \
    .csv('4_res')

spark.stop()
