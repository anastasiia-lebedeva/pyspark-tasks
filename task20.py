# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task20')
         .getOrCreate())

employee_path = 'input/employees/employees'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))

(employee_df
 .select(
    f.col('last_name'),
    f.col('salary'))
 .filter(f.col('manager_id')
         .isin(employee_df
               .filter(f.col('last_name') == 'King')
               .select(f.collect_list(f.col('employee_id')))
               .collect()[0][0]))
 .write
 .format('parquet')
 .mode('overwrite')
 .save('20_res'))

# employee_df.registerTempTable('employee')

# (spark.sql('''
#         SELECT e.last_name,
#                e.salary
#           FROM employee e
#          WHERE e.manager_id in
#                (SELECT employee_id
#                   FROM employee
#                  WHERE last_name = 'King')
#     ''')
#     .write
#     .format('parquet')
#     .mode('overwrite')
#     .save('20_res'))

spark.stop()
