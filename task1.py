# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f

# read data and start spark_session
path_to_empl = 'employees'  # '~/data_for_exam/employees/employees'
spark = SparkSession.builder \
    .master('local[*]') \
    .appName('lebedeva-spark-tsaks') \
    .getOrCreate()
empl_df = spark.read \
    .format('csv') \
    .option('header', 'true') \
    .option('sep', '\t') \
    .load(path_to_empl)

# calculate new salary
result_df = empl_df.select(
    'employee_id',
    'first_name',
    'last_name',
    'salary',
    f.round(f.col('salary') * 1.155, 2).alias('new_salary')
)

result_df.write.parquet('result')
spark.stop()
