# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql.types import StringType, StructType, StructField

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task27')
         .getOrCreate())

employee_path = 'input/employees/employees'
department_path = 'input/departments/departments'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))
department_df = (spark.read
                 .format('csv')
                 .option('header', 'true')
                 .option('sep', ',')
                 .load(department_path))

# department_df.show()

employee_df.registerTempTable('employee')
department_df.registerTempTable('department')

# can't find option to disable auto type definition
# that's why should use concat_ws or create schema with stringType in all fields
res_df = (spark.sql('''
    SELECT e.last_name, 
           d.department_id, 
           d.department_name, 
           collect_list(empty_d.department_name) `empty departments`
      FROM employee e
           LEFT JOIN department d
           ON e.department_id = d.department_id
           JOIN (SELECT d1.department_name
                   FROM department d1
                        LEFT ANTI JOIN employee e1
                        ON d1.department_id = e1.department_id) empty_d
           ON 1=1
     WHERE e.department_id is null
     GROUP BY e.last_name, d.department_id, d.department_name
'''))

df = spark.createDataFrame(
    res_df.rdd,
    StructType([StructField(res_df.columns[i], StringType(), True) for i in range(len(res_df.columns))])
)

(df
 .write
 .mode('overwrite')
 .format('csv')
 .option('header', 'true')
 .option('sep', '\t')
 .option('inferSchema', 'false')
 .saveAsTable('alebedeva_sol_27'))

spark.stop()
