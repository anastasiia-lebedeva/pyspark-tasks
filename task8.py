# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f

path_to_empl = 'input/employees/employees'

spark = SparkSession.builder \
    .master('local[*]') \
    .appName('lebedeva-spark-tasks') \
    .getOrCreate()

empl_df = spark.read \
    .format('csv') \
    .option('header', 'true') \
    .option('sep', '\t') \
    .load(path_to_empl)

empl_df = empl_df \
    .select(
        f.col('last_name'),
        f.when(f.expr('COMMISSION_PCT is not null'), f.col('COMMISSION_PCT')).otherwise(f.lit('No Comission')).alias('COMM')
    )

# empl_df.write \
#     .orc('7_res')

spark.stop()
