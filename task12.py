# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

# Expirement with Spark SQL

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task12')
         .getOrCreate())

employee_path = 'input/employees/employees'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))

employee_df.registerTempTable('employee')

(spark.sql('''
    SELECT e.last_name Employee, 
           e.employee_id as `Emp#`, 
           m.last_name Manager,
           m.employee_id `Mgr#`
      FROM employee e
           JOIN employee m
           ON e.manager_id = m.employee_id''')
 .write
 .format('parquet')
 .mode('overwrite')
 .option('compression', 'gzip')
 .save('12_res'))

spark.stop()
