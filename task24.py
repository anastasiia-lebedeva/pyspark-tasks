# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task24')
         .getOrCreate())

employee_path = 'input/employees/employees'
employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))

employee_df.registerTempTable('employee')

(spark.sql('''
        SELECT e.manager_id, 
               e.salary
          FROM employee e
         WHERE e.salary = 
               (SELECT MIN(e1.salary)
                  FROM employee e1
                 WHERE e1.manager_id = e.manager_id)
    ''')
 .write
 .format('orc')
 .saveAsTable('study_dev.alebedeva_sol_24'))

spark.stop()
