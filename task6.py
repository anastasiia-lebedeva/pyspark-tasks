# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f
from pyspark.sql.types import IntegerType

path_to_empl = 'input/employees/employees'  # '~/data_for_exam/employees/employees'
spark = SparkSession.builder \
    .master('local[*]') \
    .appName('lebedeva-spark-tasks') \
    .getOrCreate()
empl_df = spark.read \
    .format('csv') \
    .option('header', 'true') \
    .option('sep', '\t') \
    .load(path_to_empl)

res_df = empl_df \
    .select(f.concat(
        f.col('last_name'),
        f.lit(' зарабатывает '),
        f.col('salary'),
        f.lit(" каждый месяц, но хочет получать "),
        (f.col('salary') * 3).cast(IntegerType())
    ).alias('Dream_Salaries'))

res_df.write \
    .option('compression', 'gzip') \
    .parquet('6_res')

spark.stop()
