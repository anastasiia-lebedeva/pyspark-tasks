# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f

# read data and create SparkSession
path_to_empl = 'employees'  # '~/data_for_exam/employees/employees'
spark = SparkSession.builder \
    .master('local[*]') \
    .appName('lebedeva-spark-tasks') \
    .getOrCreate()
empl_df = spark.read \
    .format('csv') \
    .option('header', 'true') \
    .option('sep', '\t') \
    .load(path_to_empl)

# select last_name in `Title format` and filter by first char
result_df = empl_df \
    .select(
    f.concat(f.upper(f.substring(f.col('last_name'), 0, 1)),
             f.lower(f.expr('substring(last_name, 2, length(last_name))'))),
    f.expr('length(last_name)').alias('Length')
) \
    .filter(
    f.col('last_name').startswith('J') | f.col('last_name').startswith('A') | f.col('last_name').startswith('M')) \
    .repartition(1) \
    .sort('Length')

result_df.write \
    .option('sep', '|') \
    .csv('3_res.csv')

spark.stop()
