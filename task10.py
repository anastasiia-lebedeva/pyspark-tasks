# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f

employee_path = 'input/employees/employees'
department_path = 'input/departments/departments'

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task10')
         .getOrCreate())

employee_df = (spark.read
               .option('header', 'true')
               .option('sep', '\t')
               .format('csv')
               .load(employee_path))

department_df = (spark.read
                 .option('header', 'true')
                 .option('sep', ',')
                 .format('csv')
                 .load(department_path))

res_df = (employee_df
    .join(department_df, employee_df.DEPARTMENT_ID == department_df.DEPARTMENT_ID)
    .select(
        f.col('last_name'),
        employee_df.DEPARTMENT_ID,
        f.col('department_name')))

(res_df.write
    .option('compression', 'snappy')
    .mode('overwrite')
    .format('avro')
    .save('10_res.avro'))

spark.stop()
