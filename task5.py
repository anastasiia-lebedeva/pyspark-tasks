# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f
from pyspark.sql.types import IntegerType

# read data and start spark_session
path_to_empl = 'input/employees/employees' # '~/data_for_exam/employees/employees'
path_to_hist = 'input/job_history/job_history'
spark = SparkSession.builder \
    .master('local[*]') \
    .appName('lebedeva-spark-tasks') \
    .getOrCreate()
empl_df = spark.read \
    .format('csv') \
    .option('header', 'true') \
    .option('sep', '\t') \
    .load(path_to_empl)
hist_df = spark.read \
    .format('json') \
    .load(path_to_hist)

# join tables and add columns with years
res_df = empl_df \
    .join(hist_df, [empl_df.EMPLOYEE_ID == hist_df.employee_id, hist_df.department_id == 90]) \
    .select(empl_df.LAST_NAME, hist_df.department_id, hist_df.start_date, hist_df.end_date) \
    .withColumn('start_year', f.substring(f.col('start_date'), -2, 2).cast(IntegerType())) \
    .withColumn('end_year', f.substring(f.col('end_date'), -2, 2).cast(IntegerType())) \
    .withColumn('cur_year', f.substring(f.current_date(), 3, 2).cast(IntegerType())) 

# calculate formatted dates
res_df = res_df \
    .select(
        f.col('last_name'),
        f.when(
            f.col('start_year') > f.col('cur_year'),
            f.to_date(f.date_format(f.to_date(f.col('start_date'), 'dd.MM.yy'), '19yy-MM-dd'))
        ).otherwise(
            f.to_date(f.date_format(f.to_date(f.col('start_date'), 'dd.MM.yy'), '20yy-MM-dd'))
        ).alias('start'),
        f.when(
            f.col('end_year') > f.col('cur_year'),
            f.to_date(f.date_format(f.to_date(f.col('end_date'), 'dd.MM.yy'), '19yy-MM-dd'))
        ).otherwise(
            f.to_date(f.date_format(f.to_date(f.col('end_date'), 'dd.MM.yy'), '20yy-MM-dd'))
        ).alias('end')
    )

# round up (ceil) days / 7 for getting num of weeks and then cast to int
res_df = res_df \
    .groupBy(f.col('LAST_NAME')) \
    .agg(f.ceil(f.sum(f.datediff(res_df.end, res_df.start)) / 7).cast(IntegerType()).alias('TENSURE')) \
    .repartition(1) \
    .sort(f.desc('TENSURE')) 

res_df.write \
    .avro('5_res')

spark.stop()