# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task26')
         .getOrCreate())

department_path = 'input/departments/departments'
location_path = 'input/locations/part-00000-f3626ac3-ae1a-49d5-8aa5-760f2e41b579-c000.gz.parquet'
country_path = 'input/countries/part-00000-ebe0451d-1509-4b7e-a7fd-abeb9e5488ef-c000.snappy.orc'
department_df = (spark.read
                 .format('csv')
                 .option('header', 'true')
                 .option('sep', ',')
                 .load(department_path))
location_df = (spark.read
               .format('parquet')
               .option('compression', 'gzip')
               .load(location_path))
country_df = (spark.read
              .format('orc')
              .option('compression', 'snappy')
              .load(country_path))

department_df.registerTempTable('department')
location_df.registerTempTable('location')
country_df.registerTempTable('country')

(spark.sql('''
        SELECT c.country_id, c.country_name
          FROM country c
               LEFT JOIN location l
               ON c.country_id = l.country_id

               LEFT JOIN department d
               ON l.location_id = d.location_id
         WHERE d.department_id is null
         GROUP BY c.country_id, c.country_name
    ''')
 .write
 .format('csv')
 .option('header', 'true')
 .option('sep', ';')
 .saveAsTable('study_dev.alebedeva_sol_26'))

spark.stop()
