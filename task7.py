# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f
from pyspark.sql.types import IntegerType, StringType

import inflect

path_to_empl = 'input/employees/employees'  # '~/data_for_exam/employees/employees'
path_to_hist = 'input/job_history/job_history'
spark = SparkSession.builder \
    .master('local[*]') \
    .appName('lebedeva-spark-tasks') \
    .getOrCreate()
empl_df = spark.read \
    .format('csv') \
    .option('header', 'true') \
    .option('sep', '\t') \
    .load(path_to_empl)

res_df = empl_df \
    .withColumn('hire_year', f.substring(f.col('hire_date'), -2, 2).cast(IntegerType())) \
    .withColumn('cur_year', f.substring(f.current_date(), 3, 2).cast(IntegerType()))

res_df = res_df \
    .select(
        f.col('last_name'),
        f.when(
            f.col('hire_year') > f.col('cur_year'),
            f.to_date(f.date_format(f.to_date(f.col('hire_date'), 'dd.MM.yy'), '19yy-MM-dd'))
        ).otherwise(
            f.to_date(f.date_format(f.to_date(f.col('hire_date'), 'dd.MM.yy'), '20yy-MM-dd'))
        ).alias('date')
    )


@f.udf(StringType())
def day_to_ordinal_word(c):
    return inflect.engine().number_to_words(inflect.engine().ordinal(c.day)).title()


res_df = res_df \
    .withColumn('review_date', f.next_day(f.add_months(f.col('date'), 6), 'Mon')) \
    .select(
        f.col('last_name'),
        f.col('date').alias('hire_date'),
        f.concat(
            f.date_format('review_date', 'EEEE, \'the\' '),
            day_to_ordinal_word('review_date'),
            f.date_format('review_date', ' \'of\' MMM, yyyy')
        ).alias('REVIEW')
    )

res_df.write \
    .option('sep', '\t') \
    .csv('7_res')

spark.stop()
