# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f

spark = (SparkSession.builder
    .master('local[*]')
    .appName('alebedeva-pyspark-task11')
    .getOrCreate())

employee_path = 'input/employees/employees'
department_path = 'input/departments/departments'
location_path = 'input/locations/part-00000-f3626ac3-ae1a-49d5-8aa5-760f2e41b579-c000.gz.parquet'

employee_df = (spark.read
    .format('csv')
    .option('header', 'true')
    .option('sep', '\t')
    .load(employee_path))
department_df = (spark.read
    .format('csv')
    .option('header', 'true')
    .option('sep', ',')
    .load(department_path))
location_df = (spark.read
    .format('parquet')
    .option('compression', 'gzip')
    .load(location_path))

# employee_df.show()
# department_df.show()
# location_df.show()

res_df = (employee_df
    .join(department_df, employee_df.DEPARTMENT_ID == department_df.DEPARTMENT_ID)
    .join(location_df, department_df.LOCATION_ID == location_df.LOCATION_ID)
    .select(
         f.col('first_name'),
         f.col('job_id'),
         department_df.DEPARTMENT_ID,
         f.col('department_name'))
    .filter(f.col('city').startswith('Toronto')))

(res_df.write
    .format('parquet')
    .mode('overwrite')
    .option('compression', 'snappy')
    .save('11_res'))

spark.stop()