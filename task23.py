# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task23')
         .getOrCreate())

employee_path = 'input/employees/employees'
department_path = 'input/departments/departments'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))
department_df = (spark.read
                 .format('csv')
                 .option('header', 'true')
                 .option('sep', ',')
                 .load(department_path))

employee_df.registerTempTable('employee')
department_df.registerTempTable('department')

(spark.sql('''
        SELECT e.employee_id, 
               e.last_name, 
               e.salary
          FROM employee e
         WHERE e.salary >
               (SELECT avg(salary)
                  FROM employee)
               AND e.department_id IN 
               (SELECT department_id
                  FROM employee
                 WHERE lower(last_name) like '%u%')
    ''')
 .write
 .format('avro')
 .saveAsTable('study_dev.alebedeva_sol_23'))

spark.stop()
