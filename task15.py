# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task15')
         .getOrCreate())

employee_path = 'input/employees/employees'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))

employee_df.registerTempTable('employee')
(spark.sql('''
        SELECT last_name, hire_date
          FROM employee e
         WHERE to_date(cast(unix_timestamp(hire_date, 'dd.MM.yy') as timestamp)) > 
               (SELECT to_date(cast(unix_timestamp(hire_date, 'dd.MM.yy') as timestamp))
                  FROM employee
                 WHERE last_name = 'Davies')
    ''')
 .write
 .format('orc')
 .mode('overwrite')
 .option('compression', 'snappy')
 .save('15_res'))

spark.stop()
