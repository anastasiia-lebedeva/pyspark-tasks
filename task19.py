# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task19')
         .getOrCreate())

employee_path = 'input/employees/employees'
department_path = 'input/departments/departments'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))
department_df = (spark.read
                 .format('csv')
                 .option('header', 'true')
                 .option('sep', ',')
                 .load(department_path))

employee_df.registerTempTable('employee')
department_df.registerTempTable('department')

(spark.sql('''
        SELECT e.last_name,
               d.department_id,
               t.jobs_in_same_location
          FROM employee e
               JOIN department d
                 ON d.department_id = e.department_id
                    AND d.location_id = 1700
               JOIN (SELECT d1.location_id, COLLECT_SET(e1.job_id) jobs_in_same_location
                       FROM employee e1
                            JOIN department d1
                              ON e1.department_id = d1.department_id 
                     GROUP BY d1.location_id) t
                 ON t.location_id = 1700
    ''')
 .write
 .format('orc')
 .mode('overwrite')
 .save('19_res'))

spark.stop()
