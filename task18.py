# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession
from pyspark.sql import functions as f

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task18')
         .getOrCreate())

employee_path = 'input/employees/employees'
department_path = 'input/departments/departments'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))
department_df = (spark.read
                 .format('csv')
                 .option('header', 'true')
                 .option('sep', ',')
                 .load(department_path))

employee_df.registerTempTable('employee')
department_df.registerTempTable('department')

(spark.sql('''
        SELECT e.employee_id,
               COLLECT_LIST(t.last_name) colleague
          FROM employee e
               JOIN department d
                 ON e.department_id = d.department_id
               JOIN (SELECT last_name, department_id
                       FROM employee) t
                 ON t.department_id = d.department_id
         WHERE lower(e.last_name) like '%u%'
         GROUP BY e.employee_id
    ''')
 .write
 .format('parquet')
 .mode('overwrite')
 .save('18_res'))

spark.stop()
