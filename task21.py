# import findspark
# findspark.init('/home/nastya/spark-3.1.2-bin-hadoop3.2')

from pyspark.sql import SparkSession

spark = (SparkSession.builder
         .master('local[*]')
         .appName('alebedeva-pyspark-task21')
         .getOrCreate())

employee_path = 'input/employees/employees'
department_path = 'input/departments/departments'

employee_df = (spark.read
               .format('csv')
               .option('header', 'true')
               .option('sep', '\t')
               .load(employee_path))
department_df = (spark.read
                 .format('csv')
                 .option('header', 'true')
                 .option('sep', ',')
                 .load(department_path))

employee_df.registerTempTable('employee')
department_df.registerTempTable('department')

(spark.sql('''
        SELECT d.department_id,
               t.last_name,
               t.job_id
          FROM department d
               JOIN (SELECT e.department_id, 
                            collect_list(e.last_name) last_name,
                            collect_list(e.job_id) job_id
                       FROM employee e
                      GROUP BY e.department_id) t
                 ON t.department_id = d.department_id
         WHERE d.department_name = 'Executive'
    ''')
 .write
 .format('orc')
 .mode('overwrite')
 .save('21_res'))

spark.stop()
